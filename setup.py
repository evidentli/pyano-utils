from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='piano_utils',
    packages=find_packages(exclude=["tests"]),
    version='1.0.1',
    description='Piano Utilities for Python 3',
    author='Evidentli',
    author_email='infrastructure@evidentli.com',
    url='https://bitbucket.org/evidentli/pyano-utils',
    license='MIT',
    keywords=['piano', 'util', 'xml', 'json', 'pubmed', 'endnote', 'ris', 'csv'],
    classifiers=[],
    install_requires=requirements,
    python_requires='>=3.9'
)
