TY  - THES
AU  - Beh, Thian Thian
Y2  - 2017/01/04
Y1  - 2016
UR  - http://hdl.handle.net/11343/123144
AB  - The centromere plays a crucial role in genome inheritance - ensuring proper segregation of sister chromatids into each daughter cell. In cancer, especially in later stages of solid tumours, cells are often presented with extensive chromosomal abnormalities. However, the involvement of defective centromeres in the disease formation and progression has not been carefully studied. Hence, my PhD project aimed to investigate the role of defective centromeres in cancer progression using the NCI-60 panel of cancer cell lines. The spectrum of centromere-related abnormalities were first characterised with pan-centromeric FISH probes and then with the addition of CENP-A immunofluorescence. For HOP-92 and SN12C, cell lines showing high prevalence of functional dicentric chromosomes, expansion of single cell clones from the initial heterogeneous population was carried out to further study the involvement of dicentric chromosomes in cancer genomic instability. Neocentromere formation sites in cancer cell lines were investigated using cell lines with high prevalence of neocentric chromosomes. A neocentromere was found in T-47D which marked the first report of a neocentromere discovered in a long-established and widely used cell line, and also the first neocentromere to be reported in breast cancer. 

Separately, an antibody screen to identify antibodies recognising components of an active centromere in methanol-acetic acid stored cells was performed because thus far, antibodies that are widely used for functional centromere detection mainly worked on freshly harvested cells whereas most cytogenetic samples are stored long-term in methanol-acetic acid fixative. I found a commercially available rabbit monoclonal anti-CENP-C that worked on fixed samples and in addition, I combined three methods (FISH, immunofluorescence and mFISH) to obtain more information from the same metaphase spread. I then proceeded to test anti-CENP-C in my method, CENP-IF-cenFISH-mFISH, on dicentric- and neocentric-containing cancer cell lines and proposed other utilities for this method which I have developed.
KW  - centromere
KW  - CENP-A
KW  - CENP-C
KW  - immunofluorescence
KW  - fluorescence in situ hybridization (FISH)
KW  - multicolour FISH (mFISH)
KW  - neocentromere
KW  - dicentric
KW  - NCI-60
KW  - cancer
KW  - cytogenetics
T1  - The role of centromere defects in cancer formation and progression
L1  - /bitstream/handle/11343/123144/2016%20Dec%20Thian%20Beh%20-%20Thesis.pdf?sequence=1&isAllowed=n
PI  - 5a8c262b1dae1e0034513fa5
ER  - 
